﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAgents;

public class BasicAcademy : Academy
{
    [SerializeField]
    private Camera mainCamera;

    [SerializeField]
    private Camera agentCamera;

    public override void InitializeAcademy()
    {
        Monitor.SetActive(true);
    }

    public override void AcademyReset()
    {
    }
}