﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAgents;

public class BallAgent : Agent
{
    private Rigidbody playerRigidbody;  // 플레이어의 리지드바디

    [SerializeField]
    private Transform[] target;            // 목표의 위치

    private int random;

    public float moveForce = 5f;       // 플레이어를 이동시키는 힘
    public float turnForce = 5f;

    private bool catchTarget = false;   // 플레이어가 타겟을 잡았는지 여부
    private bool isDead = false;        // 플레이어가 플로어에서 벗어났는지 여부

    [SerializeField]
    private Camera renderCamera;

    [SerializeField]
    private Camera mainCamera;

    [SerializeField]
    private Academy academy;

    public float timeBetweenDecisionsAtInference;
    private float timeSinceDecision;

    public override void InitializeAgent()
    {
        playerRigidbody = GetComponent<Rigidbody>();
        RandomTarget();
    }

    // 에이전트가 죽었다가 다시 시작될 때의 함수
    public override void AgentReset()
    {
        // 플레이어는 훈련 유닛의 위치를 기준으로 플로어 위에 랜덤의 좌표에 위치
        transform.position = new Vector3(0, 0.79f, 0);

        isDead = false;                             // 사망상태 초기화
        catchTarget = false;
        playerRigidbody.velocity = Vector3.zero;    // 플레이어 속도 초기화

        RandomTarget(); // 타겟 초기화
    }

    // 에이전트가 수집하는 데이터 x, z 좌표에 대한 위치 속도
    public override void CollectObservations()
    {
        //Vector3 distance = target[random].transform.position - this.transform.position;

        //AddVectorObs(distance.x);
        //AddVectorObs(distance.z);

        //AddVectorObs(playerRigidbody.velocity.x);
        //AddVectorObs(playerRigidbody.velocity.z);
    }

    // 브레인이 에이전트에게 내리는 지시
    public override void AgentAction(float[] vectorAction, string textAction)
    {
        Monitor.Log("VectorAction", vectorAction, this.transform, Monitor.DisplayType.INDEPENDENT, mainCamera);

        AddReward(-0.01f); // 아무런 액션을 하지 않는 것을 방지 하기 위한 패널티
        //if (brain.brainParameters.vectorActionSpaceType == SpaceType.discrete)

        int action = Mathf.FloorToInt(vectorAction[0]);

        Vector3 targetPos = transform.position;

        switch (action)
        {
            case 0:
                // do nothing
                break;

            case 1:
                targetPos = transform.position + new Vector3(1f, 0, 0f);
                break;

            case 2:
                targetPos = transform.position + new Vector3(-1f, 0, 0f);
                break;

            case 3:
                targetPos = transform.position + new Vector3(0f, 0, 1f);
                break;

            case 4:
                targetPos = transform.position + new Vector3(0f, 0, -1f);
                break;
        }

        //Vector3 position = transform.TransformDirection(RayPerception3D.PolarToCartesian(25f, 90f));
        //Debug.DrawRay(transform.position, position, Color.red, 0f, true);
        //RaycastHit hit;
        //if (Physics.SphereCast(transform.position, 2f, position, out hit, 25f))
        //{
        //    if (hit.collider.gameObject.CompareTag("goal"))
        //    {
        //        hit.collider.gameObject.transform
        //    }
        //}

        // 타겟을 잡으면 보상하고 타겟 리셋
        if (catchTarget)
        {
            AddReward(1.0f);
            Done();
        }
        // 플레이어가 플로어 밖으로 떨어지면 종료
        else if (isDead)
        {
            AddReward(-1.0f);
            Done();
        }
    }

    private void OnTriggerEnter(Collider _collider)
    {
        if (_collider.CompareTag("goal"))
        {
            catchTarget = true;
        }
        else if (_collider.CompareTag("Finish"))
        {
            isDead = true;
        }
    }

    private void RandomTarget()
    {
        for (int i = 0; i < target.Length; i++)
        {
            target[i].tag = "wall";
            target[i].transform.localScale = new Vector3(1, 1, 1);
            target[i].GetComponent<Material>().SetColor("yellow", new Color(255, 255, 0));
        }

        random = Random.Range(0, 4);
        target[random].tag = "goal";
        target[random].transform.localScale = new Vector3(1, 2, 1);
        target[random].GetComponent<Material>().SetColor("red", new Color(255, 255, 0));
    }

    public void FixedUpdate()
    {
        WaitTimeInference();
    }

    private void WaitTimeInference()
    {
        if (renderCamera != null)
        {
            renderCamera.Render();
        }

        if (!academy.GetIsInference())
        {
            RequestDecision();
        }
        else
        {
            if (timeSinceDecision >= timeBetweenDecisionsAtInference)
            {
                timeSinceDecision = 0f;
                RequestDecision();
            }
            else
            {
                timeSinceDecision += Time.fixedDeltaTime;
            }
        }
    }
}